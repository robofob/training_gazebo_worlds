#!/usr/bin/env python3

import rospy
import yaml
import cv2
from nav_msgs.msg import OccupancyGrid
import numpy as np
from std_srvs.srv import Trigger, TriggerResponse
from geometry_msgs.msg import PoseStamped
from tf import transformations
from gazebo_msgs.srv import GetModelState, SetModelState
from gazebo_msgs.msg import ModelState
import matplotlib.pyplot as plt

class SimTeleport:
    
    def __init__(self):
        
        rospy.init_node('sim_teleport')
        
        param_file = rospy.get_param('~config')
        self.approx_radius_m = rospy.get_param('~approx_radius', 1)
        
        self.N_tries = rospy.get_param('~N_tries', 100)
        self.robot_name = rospy.get_param('~robot_name')
        
        with open(param_file, 'r') as f:            
            self.params = yaml.load(f, Loader = yaml.FullLoader)
        
        self.approx_radius_px = int(self.approx_radius_m / self.params['resolution'])
        
        # map preparations
        image_path = '/'.join(param_file.split('/')[:-1])+'/'+self.params['image']                
        self.map = cv2.imread(image_path, 0)            
                
        self.map = np.swapaxes(self.map,0,1)
        self.map = np.flip(self.map, 1)     
        
        #plt.imshow(self.map)
        #plt.show()
        
        
        print(self.map.shape)
        
        print(np.max(self.map), np.min(self.map))
        self.occ_grid_msg = OccupancyGrid()        
        self.occ_grid_msg.header.stamp = rospy.Time.now()
        self.occ_grid_msg.header.frame_id = 'map'        
        self.occ_grid_msg.data = self.map.flatten('F').astype(np.int8).tolist()        
        self.occ_grid_msg.info.map_load_time = rospy.Time.now()        
        self.occ_grid_msg.info.resolution = self.params['resolution']        
        self.occ_grid_msg.info.width = self.map.shape[0]
        self.occ_grid_msg.info.height = self.map.shape[1]        
        self.occ_grid_msg.info.origin.position.x = self.params['origin'][0]
        self.occ_grid_msg.info.origin.position.y = self.params['origin'][1]
        
        
                
        self.robot_teleport_srv = rospy.Service("~teleport_robot", Trigger, self.teleport_robot_cb)
                
        rospy.loginfo(f"[{rospy.get_name()}] waiting for /gazebo/set_model_state service...")
        rospy.wait_for_service('/gazebo/set_model_state')
        self.set_model_state_srv = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
        rospy.loginfo(f"[{rospy.get_name()}] done")
        
        self.og_pub = rospy.Publisher('~occupancy_grid', OccupancyGrid, queue_size  = 5)    
        self.pose_pub = rospy.Publisher('~teleport_pose', PoseStamped, queue_size = 1)
        rospy.Timer(rospy.Duration(0.1), self.timer_cb)
        
    def timer_cb(self, event):
        self.occ_grid_msg.header.stamp = rospy.Time.now()
        self.og_pub.publish(self.occ_grid_msg)
        
    def teleport_robot_cb(self, req):
        res = TriggerResponse()
        
        for i in range(self.N_tries):
            x_px = np.random.randint(self.approx_radius_px, self.map.shape[0] - self.approx_radius_px)
            y_px = np.random.randint(self.approx_radius_px, self.map.shape[1] - self.approx_radius_px)
            
            if not 0 in self.map[x_px - self.approx_radius_px : x_px + self.approx_radius_px,
                                 y_px - self.approx_radius_px : y_px + self.approx_radius_px]:
                #print(i, x_px, y_px)
                res.success = True
                res.message = f"found pose in {i} steps"
                pose_st = PoseStamped()
                pose_st.header.frame_id = 'map'
                pose_st.header.stamp = rospy.Time.now()
                pose_st.pose.position.x = x_px * self.params['resolution'] + self.params['origin'][0]
                pose_st.pose.position.y = y_px * self.params['resolution'] + self.params['origin'][1]
                pose_st.pose.position.z = (255 - self.map[x_px, y_px]) * self.params['z_scale']
                
                yaw = np.random.uniform(-np.pi, np.pi)
                quat = transformations.quaternion_from_euler(0, 0, yaw)
                
                pose_st.pose.orientation.x = quat[0]
                pose_st.pose.orientation.y = quat[1]
                pose_st.pose.orientation.z = quat[2]
                pose_st.pose.orientation.w = quat[3]
                
                self.pose_pub.publish(pose_st)
                
                state = ModelState()
                state.model_name = self.robot_name
                state.pose = pose_st.pose
                self.set_model_state_srv(state)
                
                break                
        
        return res
        
    def run(self):
        rospy.spin()
        
        
if __name__ == '__main__':
    st = SimTeleport()
    st.run()
