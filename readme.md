# training_gazebo_worlds
Repository with various Gazebo worlds used for developing deeplearned safety system for robot.
Old repository https://gitlab.com/robofob/custom_environments (private)

Any world can be launched as
```bash
roslaunch training_gazebo_worlds any_world.launch world_name:=<names see below>
```
## 1. Worlds
### 1.1. pillar_cage
Simpliest world of brick cage with one square pillar at center.
![pillar_cage](doc/pillar_cage.png)
### 1.2. more_pillars_cage
Simple world of brick cage with five different shaped pillars.
![more_pillars_cage](doc/more_pillars_cage.png)
### 1.3 doorways_cage
Simple world of brick cage with wall on center with doorways of different width.
![doorways_cage](doc/doorways_cage.png)

## 2. Scripts and nodes

### 2.1. teleport_robot.py
Node performing robot teleport to free zone on needed height. 
#### Prameters
 - ~robot_name (string, must be set) Name of robot model in Gazebo
 - ~config (string, must be set) Path to yaml file with world parameters (see below)
 - ~approx_radius (float, 1 [m]) Approximate radius of robot from its base_link
 - ~N_tries (int, 100) How many tries will be performed to port robot

#### Published topics
 - ~occupancy_grid ([nav_msgs/OccupancyGrid](http://docs.ros.org/en/noetic/api/nav_msgs/html/msg/OccupancyGrid.html)) Map visualization (debug)
 - ~teleport_pose ([geometry_msgs/PoseStamped](http://docs.ros.org/en/noetic/api/geometry_msgs/html/msg/PoseStamped.html)) New robot pose (debug)

#### Provided services
 - ~teleport_robot ([std_srvs/Trigger](http://docs.ros.org/en/melodic/api/std_srvs/html/srv/Trigger.html)) Ask to teleport

#### Used services
 - /gazebo/set_model_state ([gazebo_msgs/SetModelState](http://docs.ros.org/en/jade/api/gazebo_msgs/html/srv/SetModelState.html)) газебовский сервис изменения положения модели

#### Map and congig-file
World map is one-channel image. With black (0) is defined areas forbidden for robot (obstacles and so on). Other colors defining Z-pose of robot in corresponding point. Height is calculated as `z = (255 - px_value) * z_scale`.

Sctructure of param file is shown below:
```yaml
image: map.bmp # name of world map in the same dir
resolution: 0.25 # resolution [m in px]
origin: [-5.0, -5.0, 0.0] # position on left bottom corner of map in world [m]
z_scale: 0.001 # height scale
```
![map example](config/pillar_cage/map.bmp)

### 2.2. collision_detect_node
Node for detecting collisions in Gazebo

#### Parameters
 - ~links (list of strings, must be set) Links to analyze (see below how to obtain __proper__ links names)
 - ~rate_hz (double, default: 20 [hz]) Time buffer for collision analyze

#### Obtain links name
Links inside Gazebo may has different names from urdf-file of robot. For e.g. urdf link called `ritrover/wheel_fl_link` in gazebo is called `ritrover::ritrover/wheel_bl_link::ritrover/wheel_bl_link_collision`. 
To check it print Gazebo topic, collide your robot with needed link and find proper name.
```bash
gz topic -e /gazebo/default/physics/contacts
```
