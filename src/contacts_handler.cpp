/*
 * Checks collisions from gazebo topics and republish it to ROS topic
 * Based on example has taken from
 * https://github.com/osrf/gazebo/blob/gazebo11/examples/stand_alone/listener/listener.cc
*/

#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/gazebo_client.hh>

#include <iostream>
#include <ros/ros.h>
#include "training_gazebo_worlds/Contacts.h"
#include <vector>


ros::Publisher collision_pub;
std::vector<bool> last_contacts;
std::vector<std::string> links;
training_gazebo_worlds::Contacts collision_msg;

void cb(ConstContactsPtr &_msg){      
    if( _msg->contact_size() > 0){              
        for( size_t j = 0 ; j < links.size() ; j++ ){            
            for( size_t i = 0 ; i < _msg->contact_size() ; i++){  \
                if( _msg->contact(i).collision1() == links[j] or _msg->contact(i).collision2() == links[j] ){
                    collision_msg.contacts[j] = true;
                    break;
                }
            }
        }                            
    }  
}

void timerCallback(const ros::TimerEvent&){    
    collision_msg.header.stamp = ros::Time::now();
    collision_pub.publish(collision_msg); 
    for( auto& contact : collision_msg.contacts )
        contact = false;
}


int main(int _argc, char **_argv)
{  
    // Init ROS stuff
    ros::init(_argc, _argv, "collision_detector");      
    ros::NodeHandle nh_p("~");
  
    nh_p.getParam("links", links);
    double rate_hz;
    nh_p.param("rate_hz", rate_hz, 20.);
  
    if( links.size() == 0 ){
        ROS_ERROR("Error! Param links not specified, exit");
        return -1;
    }  
    for( auto& link : links){
        //ROS_WARN(link.c_str());
        collision_msg.links.push_back(link);
        collision_msg.contacts.push_back(false);
    }
  
    collision_pub = nh_p.advertise<training_gazebo_worlds::Contacts>("contacts",1);
 
    // Init Gazebo transport
    gazebo::client::setup(_argc, _argv);
    gazebo::transport::NodePtr node(new gazebo::transport::Node());
    node->Init();
    gazebo::transport::SubscriberPtr sub = node->Subscribe("/gazebo/default/physics/contacts", cb);  
    
    // do what have to be done
    ros::Timer timer = nh_p.createTimer(ros::Duration(1.0/rate_hz), timerCallback);  
    ros::spin();
    gazebo::client::shutdown();
}
